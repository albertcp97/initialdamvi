﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coche : MonoBehaviour
{//evitar que el coche vaya dando vuelcos al minimo giro, se soluciona poniendo un centro de grabedead mas bajo. A veces no se girara al volcar, por los colliders de las ruedas.
    public Vector3 centerOfMass = Vector3.zero;

   //fuerza motor y radio motor
    public float FMotor;
    public float FRadius;

    //luces traseras al frenar se intensificaran
    public GameObject l1;
    public GameObject l2;

    //ruedas y wheelcollider (wheel collider no estan en las ruedas)
    public GameObject rueda1;
    public GameObject rueda2;
    public WheelCollider DI;
    public WheelCollider DD;
    public WheelCollider AI;
    public WheelCollider AD;

    private new AudioSource audio;

    public AudioClip[] playList;

    //velocimetro, al augmentar la velocidad este rotara
    public GameObject vel;

    //ruedas sin wheel collider, para dar sensacion de que mueven al coche
    public GameObject DeIz;
    public GameObject DeDe;

    int caso = 0;
    private Boolean cuentaAtras = false;
    private Boolean Contanct = false;

    private int check;
    private int pos;

    // Start is called before the first frame update
    void Start()

    {//iniciamos el centro de massa para que no vuelque al minimo giro
        this.GetComponent<Rigidbody>().centerOfMass = centerOfMass;
        check = 0;
        pos = 0;
        audio = GetComponent<AudioSource>();
        ponerCancion();


    }

    private void ponerCancion()
    {
        audio.clip = playList[pos];
        audio.Play();
        StopAllCoroutines();
    }

    //si las reudas estan en contacto con el suelo pueden dar fuerza al coche
    public void EnContacto()
    {
        Contanct = true;
    }

    //sino inciciara la corrutina para reposicionar el coche a los 5 segundos
    public void Cuenta()
    {
        cuentaAtras = true;
    }

    //si estan en el suelo parara la corrutina
    public void CuentaOff()
    {
        cuentaAtras = false;
    }

    // Update is called once per frame

       
    void FixedUpdate()
    {//obtener que teclas estamos precionando, estas haran que el coche acelere o gire las ruedas
        float v = Input.GetAxis("Vertical") * FMotor;
        float h = Input.GetAxis("Horizontal") * FRadius;
        if (v > 0 && !Input.GetKey(KeyCode.Space))
        {
            if (caso != 1)
            {
                pos = 1;
                caso = 1;
                ponerCancion();
            }
        }
        else
        {
            if (caso != 0&& !Input.GetKey(KeyCode.Space)) { 
            pos = 0;
                caso = 0;
            ponerCancion();
        }
        }

        Debug.Log(caso);
        //Debug.Log(AI.motorTorque);

        //si esta en contacto con el suelo, puede acelerar
        if (Contanct)
        {
            AI.motorTorque = v;
            AD.motorTorque = v;
            StopAllCoroutines();
            //this.GetComponent<Rigidbody>().AddRelativeForce(0, 0, v);
        

        }
        else
        {//sino iniciara la cuenta atras para reposicionar el coche
            if (!cuentaAtras)
            {
                StartCoroutine(reinciar());
            }
            
        }
        //girar ruedas
        DeDe.transform.localEulerAngles = new Vector3(0, h, 90);
        DeIz.transform.localEulerAngles = new Vector3(0, h, 90);
        rueda1.transform.localEulerAngles = new Vector3(0, 90+h, 0);
        rueda2.transform.localEulerAngles = new Vector3(0, 90+h, 0);
        DD.steerAngle = h;
        DI.steerAngle = h;


        //Iniciar el derrape
        if (Input.GetKey(KeyCode.Space))
        {
           //cambiamos la freccion haciendo que patine
            WheelFrictionCurve sd = AI.sidewaysFriction;

            sd.stiffness = 0.2f;
            AI.sidewaysFriction = sd;
            AD.sidewaysFriction = sd;
            sd = new WheelFrictionCurve();
            sd = AI.forwardFriction;
            //augmentamos la friccion para que no puedas acelerar mas
            sd.extremumSlip = 10000;

            AI.forwardFriction = sd;
            AD.forwardFriction = sd;
            //augmentamos intensidad luces traceras
            l1.GetComponent<Light>().intensity = 3;
            l2.GetComponent<Light>().intensity = 3;
            if (caso != 2)
            {
                pos = 2;
                caso = 2;
                ponerCancion();
            }
            
        }
        else
        {
            //restablecemos valores al dejar de presionar espacio
            WheelFrictionCurve sd = AI.sidewaysFriction;

            sd.stiffness = 0.8f;
            AI.sidewaysFriction = sd;
            AD.sidewaysFriction = sd;
            l1.GetComponent<Light>().intensity = 1.5f;
            l2.GetComponent<Light>().intensity = 1.5f;
            sd = new WheelFrictionCurve();
            sd = AI.forwardFriction;
            sd.extremumSlip = 200;
            AI.forwardFriction = sd;
            AD.forwardFriction = sd;
        }

        //para controlar la posicion del velocimetro. Obtenemos velocidad, la pasamos a km/h y rotamos segun el maximo de velocidad(220)
        float max = (this.GetComponent<Rigidbody>().velocity.magnitude * 60 * 60 / 1000 / 220)*100;
        vel.transform.localEulerAngles = new Vector3(max, 88.755f, 1.315f);

       

        //ponemos siempre a false el contacto de las ruedas con el suelo
        Contanct = false;
    }

    //esta funcion reposicionara el coche si este esta mucho tiempo sin tocar el suelo
    private IEnumerator reinciar()
    {
        yield return new WaitForSeconds(5f);

        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 2, this.transform.position.z);
        this.transform.localEulerAngles = new Vector3(0, 0, 0);
    }

    //miramos si pasa por los checkpoints, al pasar por el que le corresponde llamara a la funcion que incrementa el estado de la carrera(esta en Partida.cs)
    public void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.name=="1")
        {
         
            if (check == 0)
            {
                check++;
                //algo que remarcar, puedes llamar a funciones de los padres sin usar delegados
                this.transform.parent.SendMessage("incrementar");
            }
        }

        if (collision.gameObject.name == "2")
        {
          
            if (check == 1)
            {
                check++;
                this.transform.parent.SendMessage("incrementar");
            }

        }
        if (collision.gameObject.name == "3")
        {
          
            if (check == 2)
            {
                check++;
                this.transform.parent.SendMessage("incrementar");
            }

        }

        if (collision.gameObject.name == "4")
        {
           
            if (check == 3)
            {
                check++;
                this.transform.parent.SendMessage("incrementar");
            }
        }
        if (collision.gameObject.name == "5")
        {
          
            if (check == 4)
            {
                check++;
                this.transform.parent.SendMessage("incrementar");
            }
        }

        if (collision.gameObject.name == "6")
        {
           
            if (check == 5)
            {
                check++;
                this.transform.parent.SendMessage("incrementar");
            }

        }

        if (collision.gameObject.name == "7")
        {
           
            if (check == 6)
            {
                check++;
                this.transform.parent.SendMessage("incrementar");
            }
        }

        if (collision.gameObject.name == "8")
        {
            
            if (check == 7)
            {
                check++;
                this.transform.parent.SendMessage("incrementar");
            }
        }

        if (collision.gameObject.name == "Meta")
        {
           
            if (check == 8)
            {
                check=0;
                this.transform.parent.SendMessage("incrementar");
            }
        }

       
        
       
    }
}
