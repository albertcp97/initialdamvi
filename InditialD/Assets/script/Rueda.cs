﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rueda : MonoBehaviour
{
    private WheelHit hit;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {//saber si esta en contacto con el suelo, si pasan mas de 5 segundos sin tocar el suelo el coche se reiniciara su posicion actual para que puedas volver a correr
        if (this.GetComponent<WheelCollider>().GetGroundHit(out hit))
        {
            this.transform.parent.SendMessage("EnContacto");

            //si entra en contacto, parara la corrutina que reposicionara el coche a los 5 segundos
            this.transform.parent.SendMessage("CuentaOff");
        }
        else
        {//reposicionara el coche a los 5 segundos llamando a la funcion del coche
            this.transform.parent.SendMessage("Cuenta");
        }
        
    }
}
